// const liveServer = require("live-server");

// const params = {
//     port: 80, // Set the server port. Defaults to 8080.
//     host: "0.0.0.0", // Set the address to bind to. Defaults to 0.0.0.0 or process.env.IP.
//     root: "/", // Set root directory that's being served. Defaults to cwd.
//     open: false, // When false, it won't load your browser by default.
//     ignore: 'scss,my/templates', // comma-separated string for paths to ignore
//     file: "index.html", // When set, serve this file for every 404 (useful for single-page applications)
//     wait: 1000, // Waits for all changes, before reloading. Defaults to 0 sec.
//     // mount: [['/components', './node_modules']], // Mount a directory to a route.
//     logLevel: 2, // 0 = errors only, 1 = some, 2 = lots
//     middleware: [function(req, res, next) { next(); }] // Takes an array of Connect-compatible middleware that are injected into the server middleware stack
// };
// liveServer.start(params);

const fs = require('fs')
const path = require('path')
const express = require('express')
const serveStatic = require('serve-static')

const app = express()

app.use(express.static('public'));
// app.use(serveStatic('public/ftp', {'index': ['index.html', 'index.htm']}))

// app.get('/images/:file?', (req, res) => {
//   const _file = req.params.file ? req.params.file : 'index'
//   console.log('file: ', req.params.file)
//   console.log('_file: ', _file)
//   const file = path.join(__dirname, 'public/images/'+req.params.file)
//   console.log('sendFile: ', file)

//   res.sendFile(file)
// })
app.get('/:file?', (req, res) => {
  const _file = req.params.file ? req.params.file : 'index'
  console.log('file: ', req.params.file)
  console.log('_file: ', _file)
  const file = path.join(__dirname, 'public/'+req.params.file+'.html')
  console.log('sendFile: ', file)

  res.sendFile(file)
})

app.listen(80)